import Vue from 'vue'
import App from './App.vue'
import HelloComponent from './HelloComponent.vue'

new Vue({
  el: '#app',
  components: {
    HelloComponent
  }
  // render: h => h(App)   -- This was the line. App.vue was overwriting my own component 
})
