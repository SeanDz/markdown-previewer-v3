window.onload = getMyLocation

function getMyLocation () {
 if (navigator.geolocation) {
   navigator.geolocation.getCurrentPosition(displayLocation, displayError)
 } else {
   alert("No GeoLocation Support")
 }
}


function computeDistance(startCoords, destCoords) {
  var startLatRads = degreesToRadians(startCoords.latitude);
  var startLongRads = degreesToRadians(startCoords.longitude);
  var destLatRads = degreesToRadians(destCoords.latitude);
  var destLongRads = degreesToRadians(destCoords.longitude);

  var Radius = 6371; // radius of the Earth in km
  var distance = Math.acos(Math.sin(startLatRads) * Math.sin(destLatRads) +
      Math.cos(startLatRads) * Math.cos(destLatRads) *
      Math.cos(startLongRads - destLongRads)) * Radius;
  return distance;
}

function degreesToRadians(degrees) {
  var radians = (degrees * Math.PI)/180;
  return radians;
}

var ourCoords = {
  latitude: 47.624851,
  longitude: -122.52099
}



function displayLocation (position) {
  var longitude = position.coords.longitude
  var latitude = position.coords.latitude

  var div = document.getElementById("location") // div is now a reference to the selector
  div.innerHTML = `You are at Latitude: ${latitude}, Longitude: ${longitude}` //now I can access/set the selector's target's value

  var km = computeDistance(position.coords, ourCoords)

  var distance = document.getElementById("distance")

  distance.innerHTML = "You are " +km+ " km from the WS HQ"

  showMap(position.coords)

}


function displayError(errorArgument) {
  var errorTypes = {
    0: "Unknown error",
    1: "Permission denied by user",
    2: "Position not available",
    3: "Request Timeout"
  }
  var errorMessage = errorTypes[errorArgument.code] // This is where the argument "enters the picture." So it's not always manually typed at all. It is often just used like this.
  if (errorArgument.code === 0 || errorArgument.code === 2) {
    errorMessage = errorMessage + " " + errorArgument.message
  }
  var div = document.getElementById("location")
  div.innerHTML = errorMessage
}

var map

function showMap(coords) {

    var googleLatAndLong = new google.maps.LatLng(coords.latitude, coords.longitude)

    var mapOptions = {
      zoom: 10,
      center: googleLatAndLong,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    var mapDiv = document.getElementById('map') // look at how line 79 and 81 connect. This is the key thing I must learn

  map = new google.maps.Map(mapDiv, mapOptions)

}




