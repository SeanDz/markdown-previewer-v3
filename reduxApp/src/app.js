import {createStore} from 'redux'


// Step 3 define reducers
const reducer = function(state={books:[]}, action) {
  switch(action.type){
    case "post_book":
    // let books = state.books.concat(action.payload)
    //   return {books}
      return {books:[...state.books, ...action.payload]}
    break

    case "delete_book":
      // Create copy of current array of books
      const currentBookToDelete = [...state.books]
      const indexToDelete = currentBookToDelete.findIndex(
        function(book){
          return book.id === action.payload.id
        }
      )
      //use slice to remove the book at the specified index & return the new state
      return {books: [...currentBookToDelete.slice(0, indexToDelete), ...currentBookToDelete.slice(indexToDelete + 1)]}
      break

    case "update_book":
    //create copy of current array of books
    const currentBookToUpdate = [...state.books]
    const indexToUpdate = currentBookToUpdate.findIndex(
      function(book){
        return book.id === action.payload.id
      }
    )
    // Create a new book object with the new values and with the same array inde of the item we want to replace. To achieve this we will use ...spread but could have also used concat method
    const newBookToUpdate = {
      ...currentBookToUpdate[indexToUpdate],
      title: action.payload.title
    }
  }
  return state
}

// Step 1 create the store
const store = createStore(reducer)

// subscribe method shows current state
store.subscribe(function () {
  console.log('current state: ', store.getState())
  // console.log('current price: ', store.getState()[1].price)
})

// Step 2 create and dispatch actions
store.dispatch({
  type: "post_book",
  payload: [
    {
     id: 1,
     title: 'this is the book title',
     description: 'this is the book description',
     price: 33.33
     },
    {
      id: 2,
      title: 'this is the 2nd book title',
      description: 'this is the 2nd book description',
      price: 50
    }
  ]
})

// DISPATCH a 2nd action

store.dispatch({
  type: "delete_book",
  payload:
    {
      id: 1}
})

// UPDATE a book
store.dispatch({
  type: 'update_book',
  payload: {
    id: 2,
    titdle: 'Learn React in 24h'
  }
})











