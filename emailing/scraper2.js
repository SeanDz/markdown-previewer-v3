/**
 * Created by Owner on 6/19/2017.
 */

var Nightmare_module = require('nightmare')
var nightmare_instance = Nightmare_module({
  show: true // SHOWS the browser
})

var search_term = 'plumber las vegas'

// do a search for the target term

nightmare_instance
  .viewport(1200, 900)
  .goto('https://duckduckgo.com')
  .type('#search_form_input_homepage', search_term)
  .click('#search_button_homepage')
  .wait('.result__a')


  // todo try copying a single domain name first

  .evaluate(function() {
    document.querySelectorAll('.result__url__domain').forEach(function (singleEl) {
      console.log(singleEl.innerText)
    })
  })

  // .then(function (resultObject) {
  //   console.log(resultObject)
  // })

  // todo copy every domain on the SERP


  .then(function (resultObject) {
    console.log(resultObject)
  })
  // .catch(function(errorObject) {
  //   console.error('Search failed. Reason: ', errorObject)
  // })