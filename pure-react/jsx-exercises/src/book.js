/**
 * Created by Owner on 6/25/2017.
 */

import React from 'react'
import ReactDOM from 'react-dom'

var Book = React.createClass({
  render: function () {
    return (
      <div>
      <div className="title">The Title</div>
      <div className="author">The Author</div>
      <ul className="stats">
        <li className="rating">5 Stars</li>
        <li className="isbn">12-345678-910</li>
      </ul>
    </div>
    )
  }
})

ReactDOM.render(
  <Book />,
  document.getElementById('root')
)