import React from 'react'
import ReactDOM from 'react-dom'

var HelloWorld = React.createClass({
  render: function () {
    return (
      <div>Hello World</div>
      )
  }
})

var Hello = React.createClass({
  render: function () {
    return (
      <div>
        <span>Hello</span>
      </div>
    )
  }
})

var World = React.createClass({
  render: function () {
    return (
      <div>
        <span>World</span>
      </div>
    )
  }
})

ReactDOM.render(
  <div>
  <Hello /> <World />
  </div>,
  document.querySelector('#root')
)